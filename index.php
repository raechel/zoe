<?php
if (isset($_GET['p'])) {
    switch ($_GET['p']) {
        case 'pilots':
            $current = "pilots.php";
            break;
        case 'entrepreneurs':
            $current = "entrepreneurs.php";
            break;
        default:
            $current = "home.php";
            break;
    }
} else {
    $current = "home.php";
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Zoe</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
        <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="assets/vendor/feather/css/feather.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/fontawesome-all.min.css" rel="stylesheet">
        <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
        <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="assets/vendor/owl.carousel/owl.transitions.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">
    </head>

    <body>
        <!--========================================================
                                HEADER
        =========================================================-->
        <header id="header" class="fixed-top ">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-xl-9 d-flex align-items-center justify-content-between">
                        <h1 class="logo"><a href="index.html"><img src="assets/img/Captura126.PNG" style="width: 100%;"/></a></h1>
                        <nav class="nav-menu d-none d-lg-block">
                            <ul>
                                <li><a class="<?php echo ($current == 'home.php' ? 'active' : '') ?>" href="index.php">Inicio</a></li>
                                <li><a class="<?php echo ($current == 'entrepreneurs.php' ? 'active' : '') ?>" href="index.php?p=entrepreneurs">Emprendedores</a></li>
                                <li><a class="<?php echo ($current == 'pilots.php' ? 'active' : '') ?>" href="index.php?p=pilots">Pilotos</a></li>
                                <li><a href="index.php" style="color:#000;">Mapa</a></li>
                                <li><a href="index.php" style="color:#000;">Blog</a></li>
                                <li><a href="index.php" style="color:#000;">Recursos</a></li>
                                <li><a href="index.php" style="color:#000;">Contacto</a></li>
                            </ul>
                        </nav>
                        <a href="index.html" class="get-started-btn scrollto">&iexcl;Descargarla ya!</a>
                    </div>
                </div>
            </div>
        </header>

        <?php include $current ?>

        <!--========================================================
                                FOOTER
        =========================================================-->
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <a href="index.html"><img src="assets/img/Captura126.PNG" style="width: 100%;"/></a>
                    </div>
                    <div class="col-md-2">
                        <p style="margin-bottom: 0px;">ZOE</p>
                        <p style="margin-bottom: 0px;">Emprendedores</p>
                        <p style="margin-bottom: 0px;">Piloto</p>
                        <p style="margin-bottom: 0px;">Preguntas frecuentes</p>
                        <p style="margin-bottom: 0px;">Recursos</p>                  
                    </div>
                    <div class="col-md-2">
                        <p style="margin-bottom: 0px;">Contacto</p>
                        <p style="margin-bottom: 0px;">Centro de ayuda</p>
                        <p style="margin-bottom: 0px;">Termino y condiciones</p>
                        <p style="margin-bottom: 0px;">politicas de privacidad y seguridad</p>
                        <p style="margin-bottom: 0px;">Programas de lealtad</p>
                    </div>
                    <div class="col-md-2">
                        <p>Siguenos en:</p>
                        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                    </div>
                    <div class="col-md-1">
                        <img src="../assets/img/App_Store.png" style="width: 100%;height: 30px;"/>
                    </div>
                    <div class="col-md-1">
                        <img src="../assets/img/Google _Play.png" style="width: 100%;height: 30px;"/>           
                    </div>
                    <div class="col-md-2">
                        <a href="index.html"><img src="assets/img/Captura126.PNG" style="width: 100%;"/></a>
                    </div>

                </div>

            </div>
        </footer>

        <!-- Vendor JS Files -->
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="assets/vendor/php-email-form/validate.js"></script>
        <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
        <script src="assets/vendor/counterup/counterup.min.js"></script>
        <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="assets/vendor/venobox/venobox.min.js"></script>
        <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main.js"></script>
    </body>

</html>