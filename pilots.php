<section id="heroPiloto" class="d-flex flex-column justify-content-center">
    <div class="container">
        <div class="row justify-content-left">
            <div class="col-xl-2"></div>
            <div class="col-xl-8">
                <h1 style="text-align: center;"><b>&Uacute;nete como piloto</b></h1>
                <p style="text-align: center;"><b>&iquest;Deseas tener la libertad de establecer tus propias</b></p>
                <p style="text-align: center;"><b>horas de trabajo con un sistemas de tarifas</b></p>
                <p style="text-align: center;"><b>competitivas y transparentes?</b></p>
            </div>
        </div>
    </div>
</section>
<main id="main">
    <div class="row">
        <div class="col-md-6" style="background: #fff;padding: 10px;">
            <div class="row">
                <div class="col-md-12" style="color: #000;padding-left: 220px;padding-right: 100px;padding-top: 90px;">
                    <h2 style="text-align: center"><b style="color:#fd6c05;">&iquest;Com&oacute; funciona?</b></h2>
                    <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text</p>
                </div>
            </div>
        </div>
        <div class="col-md-6" >
            <div class="row">
                <div class="col-md-12" id="secursPoli">
                </div>
            </div>
        </div>
    </div>
    <section id="cta" class="ctaPoli">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center text-lg-left" style="padding: 50px;">
                </div>
                <div class="col-lg-6 text-center text-lg-left" style="padding: 50px;color: #fff">
                    <h3 style="text-align: center"><b>&iquest;C&oacute;mo afiliarse?</b></h3>
                    <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    <p style="text-align: center"><a href="index.html" class="get-started-btn scrollto" style="background: #ffffff05;color: #fff;border: 1px solid rgb(255 255 255);">&iexcl;Aplica ahora!</a></p>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col-md-6" style="background: #fd6c05;padding: 10px;">
            <div class="row">
                <div class="col-md-12" style="color: #fff;padding-left: 220px;padding-right: 100px;padding-top: 65px;">
                    <h2 style="text-align: center"><b style="color:#fff;">Requisitos para ser un piloto</b></h2>
                    <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text</p>
                    <p style="text-align: center"><a href="index.html" class="get-started-btn scrollto" style="background: #ffffff05;color:#fff;border: 1px solid #fff;">Lee los requisitos</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-6" >
            <div class="row">
                <div class="col-md-12" id="secursUni">
                </div>
            </div>
        </div>
    </div>
    <section id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-stretch">
                    <div class="icon-box">
                        <div class="icon"><img src="../assets/img/vectores seccion pilotos-01.png" style="width:165px;"/></div>
                        <h3 style="text-align: center;color:#fd6c05;">T&eacute;rminos y condiciones</h3>
                        <p style="text-align: center;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <br>
                        <p style="text-align: center"><a href="index.html" class="get-started-btn scrollto" style="background: #fd6c05;color: #fff;">Leer aqui</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col-md-3" >
            <div class="row">
                <div class="col-lg-12 text-center securs4" style="padding: 50px;">
                    <br><br><br><br><br>
                    <h3 style="text-align: center;color: #fff;"><b>Preguntas frecuentes</b></h3>
                </div>
            </div>
        </div>
        <div class="col-md-3" >
            <div class="row">
                <div class="col-lg-12 text-center securs7" style="padding: 50px;">
                    <br><br><br><br><br>
                    <h3 style="text-align: center;color: #fff;"><b>Acceso a videos de tutoriales</b></h3>
                </div>
            </div>
        </div>
        <div class="col-md-3" >
            <div class="row">
                <div class="col-lg-12 text-center securs5" style="padding: 50px;">
                    <br><br><br><br><br>
                    <h3 style="text-align: center;color: #fff;"><b>Buenas pr&aacute;cticas de atenci&oacute;n al cliente</b></h3>
                </div>
            </div>
        </div>
        <div class="col-md-3" >
            <div class="row">
                <div class="col-lg-12 text-center securs6" style="padding: 50px;">
                    <br><br><br><br><br>
                    <h3 style="text-align: center;color: #fff;"><b>Otros</b></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6" >
            <div class="row">
                <div class="col-md-12" id="secursPoli2">
                </div>
            </div>
        </div>
        <div class="col-md-6" style="background: #fff;padding: 10px;">
            <div class="row">
                <div class="col-md-12" style="color: #000;padding-left: 100px;padding-right: 100px;padding-top: 65px;">
                    <h2 style="text-align: center"><b style="color:#fd6c05;">&iexcl;Conduce con nosotros!</b></h2>
                    <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text</p>
                    <p style="text-align: center"><a href="index.html" class="get-started-btn scrollto" style="background: #ffffff05;color: #fd6c05;border: 1px solid #fd6c05;">Afiliate ya</a></p>
                </div>
            </div>
        </div>
    </div>
</main>
