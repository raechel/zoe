<section id="heroEmprendedores" class="d-flex flex-column justify-content-center">
    <div class="container">
        <div class="row justify-content-left">
            <div class="col-xl-2"></div>
            <div class="col-xl-8">
                <h1 style="text-align: center;"><b>&iquest;Pensando en crecer?</b></h1>
                <p style="text-align: center;"><b>S&eacute; parte de nuestros socios estrat&eacute;gicos. &Uacute;nete a</b></p>
                <p style="text-align: center;"><b>ZOE para llegar a m&aacute;s clientes, retomar el control</b></p>
                <p style="text-align: center;"><b>de tus &oacute;rdenes y aumentara tus ventas.</b></p>
            </div>
        </div>
    </div>
</section>
<main id="main">
    <section id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="text-align: center;">
                    <h1><b style="color: hsl(351deg 92% 48%);;">&iquest;C&oacute;mo funciona?</b></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="icon-box">
                        <div class="icon"><img src="../assets/img/vectores seccion emprendedores-0 1.png" style="width:165px;"/></div>
                        <h3 style="text-align: center;">Lorem Ipsum</h3>
                        <p style="text-align: center;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
                    <div class="icon-box">
                        <div class="icon"><img src="../assets/img/vectores seccion emprendedores-02.png" style="width:165px;"/></div>
                        <h3 style="text-align: center;">Lorem Ipsum</h3>
                        <p style="text-align: center;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
                    <div class="icon-box">
                        <div class="icon"><img src="../assets/img/vectores seccion emprendedores-03.png" style="width:165px;"/></div>
                        <h3 style="text-align: center;">Lorem Ipsum</h3>
                        <p style="text-align: center;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>       
            </div>
    </section>
    <section id="cta" class="ctaAfiliarse">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center text-lg-left" style="padding: 50px;">
                </div>
                <div class="col-lg-6 text-center text-lg-left" style="padding: 50px;color: #fff">
                    <h3 style="text-align: center"><b>&iquest;C&oacute;mo afiliarse?</b></h3>
                    <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    <p style="text-align: center"><a href="index.html" class="get-started-btn scrollto" style="background: #ffffff05;color: #fff;border: 1px solid rgb(255 255 255);">Afiliate ya</a></p>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col-md-6" style="background: #fff;padding: 10px;">
            <div class="row">
                <div class="col-md-12" style="color: #000;padding-left: 220px;padding-right: 100px;padding-top: 65px;">
                    <h2 style="text-align: center"><b style="color:hsl(351deg 92% 48%);">Conoce los requisitos</b></h2>
                    <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text</p>
                    <p style="text-align: center"><a href="#about" class="get-started-btn scrollto" style="background: #ffffff05;color:#fd6c05;border: 1px solid rgb(253 108 5);">Requisitos</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-6" >
            <div class="row">
                <div class="col-md-12" id="secursRequi">
                </div>
            </div>
        </div>
    </div>
    <section id="cta" class="ctaTer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center" style="padding: 50px;">
                    <h3 style="text-align: center;color: #fff;"><b>T&eacute;rminos y condiciones</b></h3>
                    <p style="text-align: center"><a href="index.html" class="get-started-btn scrollto" style="background: #fff;color: #fd6c05;">Leer aqui</a></p>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col-md-4" >
            <div class="row">
                <div class="col-lg-12 text-center securs1" style="padding: 50px;">
                    <br><br><br><br><br>
                    <h3 style="text-align: center;color: #fff;"><b>Preguntas frecuentes</b></h3>
                </div>
            </div>
        </div>
        <div class="col-md-4" >
            <div class="row">
                <div class="col-lg-12 text-center securs3" style="padding: 50px;">
                    <br><br><br><br><br>
                    <h3 style="text-align: center;color: #fff;"><b>Acceso a videos de tutoriales</b></h3>
                </div>
            </div>
        </div>
        <div class="col-md-4" >
            <div class="row">
                <div class="col-lg-12 text-center securs2" style="padding: 50px;">
                    <br><br><br><br><br>
                    <h3 style="text-align: center;color: #fff;"><b>Gu&iacute;as para emprendedores</b></h3>
                </div>
            </div>
        </div>
    </div>
    <section class="d-flex flex-column justify-content-center" style="padding: 100px 0;">
        <div class="row">
            <div class="col-md-12" style="color: #000;">
                <h2 style="text-align: center"><b style="color:hsl(351deg 92% 48%);">&iexcl;Crece con nosotros!</b></h2>
                <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text</p>
                <p style="text-align: center"><a href="#about" class="get-started-btn scrollto" style="background: #ffffff05;color:#fd6c05;border: 1px solid rgb(253 108 5);">Afiliate ya</a></p>
            </div>
        </div>
    </section>
</main>