<section id="hero" class="d-flex flex-column justify-content-center">
    <div class="container">
        <div class="row justify-content-left">
            <div class="col-xl-2"></div>
            <div class="col-xl-8">
                <h1 style="text-align: left;"><b>&iquest;Qu&eacute; pedir hoy?</b></h1>
                <p style="text-align: left;"><b>Ordena de tus restaurantes favoritos,</b></p>
                <p style="text-align: left;"><b>bebida,transporte,medicinas supermecados</b></p>
                <p style="text-align: left;"><b> y mas en un solo lugar.</b></p>
            </div>
        </div>
    </div>
</section>
<main id="main">
    <div class="row">
        <div class="col-md-6" style="background: #ed0b28;padding: 10px;">
            <div class="row">
                <div class="col-md-12" style="color: #fff;padding-left: 220px;padding-right: 100px;padding-top: 65px;">
                    <h2 style="text-align: center"><b>&iquest;Como funciona?</b></h2>
                    <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text</p>
                    <p style="text-align: center"><a href="#about" class="get-started-btn scrollto" style="background: #ffc107;color: #000;">Leer aqui</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-6" >
            <div class="row">
                <div class="col-md-12" id="securs">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6" id="theer">
            </div>
            <div class="col-md-6">
            </div>
            <div class="col-md-6">
                <br><br><br><br>
                <h1 style="color:#fd6c05;text-align: center;"><b>EMPRENDEDORES</b></h1>
                <br>
                <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p style="text-align: center"><a href="index.html" class="get-started-btn scrollto" style="background: #fd6c05;color: #000;">Leer aqui</a></p>
                <br><br><br><br>
            </div>
        </div>
    </div>
    <section id="cta" class="cta">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center text-lg-left" style="padding: 50px;">
                    <h3 style="text-align: center"><b>PILOTOS</b></h3>
                    <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    <p style="text-align: center"><a href="index.html" class="get-started-btn scrollto" style="background: #fff;color: #000;">Leer aqui</a></p>
                </div>
            </div>
        </div>
    </section>
    <section id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="text-align: center;">
                    <h1><b style="color:#fd6c05;">&iquest;D&oacute;nde</b><b style="color:#000;"> se descarga la APP?</b></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="icon-box">
                        <div class="icon"><img src="../assets/img/01.png" style="width:165px;"/></div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
                    <div class="icon-box">
                        <div class="icon"><img src="../assets/img/02.png" style="width:100px;"/></div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
                    <div class="icon-box">
                        <div class="icon"><img src="../assets/img/03.png" style="width:115px;"/></div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>       
            </div>
            <br>
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" style="justify-content: center;">
                    <div class="icon-box">
                        <h3 style="color: #fd6c05;"><b>Descarga ZOE aqui</b></h3>
                        <div class="icon"><i class="fas fa-angle-double-down"></i></div>
                        <div class="row">
                            <div class="col-md-6">
                                <img src="../assets/img/App_Store.png" style="width: 90%;height: 40px;"/>
                            </div>
                            <div class="col-md-6">
                                <img src="../assets/img/Google _Play.png" style="width: 90%;height: 40px;"/>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section id="services" class="services">
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <h1><b style="color:hsl(0deg 0% 45%);">&iquest;Qu&eacute; dicen nuestros clientes?...</b></h1>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12">
                <!--Carousel Wrapper-->
                <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
                    <!--Indicators-->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-2" data-slide-to="1"></li>
                        <li data-target="#carousel-example-2" data-slide-to="2"></li>
                    </ol>
                    <!--/.Indicators-->
                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="view">
                                <img class="d-block w-100" src="../assets/img/member-01.jpg" style="margin-left: 300px; border-radius: 100% !important;width: 10% !important;"
                                     alt="First slide">
                                <div class="mask rgba-black-light"></div>
                            </div>
                            <div class="carousel-caption" style="right: 20%;bottom: 0px;left: 20%;right: 20%;bottom: 0px;left: 20%;">
                                <h3 class="h3-responsive" style="color: #000;margin-top: 0px;margin-bottom: 0px;padding-left: 285px;">What is Lorem Ipsum?</h3>
                                <p style="margin-bottom: 0px;color: #000;padding-left: 285px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="view">
                                <img class="d-block w-100" src="../assets/img/member-02.jpg" style="margin-left: 300px; border-radius: 100% !important;width: 10% !important;"
                                     alt="Second slide">
                                <div class="mask rgba-black-strong"></div>
                            </div>
                            <div class="carousel-caption" style="right: 20%;bottom: 0px;left: 20%;right: 20%;bottom: 0px;left: 20%;">
                                <h3 class="h3-responsive" style="color: #000;margin-top: 0px;margin-bottom: 0px;padding-left: 285px;">What is Lorem Ipsum?</h3>
                                <p style="margin-bottom: 0px;color: #000;padding-left: 285px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="view">
                                <img class="d-block w-100" src="../assets/img/member-03.jpg" style="margin-left: 300px; border-radius: 100% !important;width: 10% !important;"
                                     alt="Third slide">
                                <div class="mask rgba-black-slight"></div>
                            </div>
                            <div class="carousel-caption" style="right: 20%;bottom: 0px;left: 20%;right: 20%;bottom: 0px;left: 20%;">
                                <h3 class="h3-responsive" style="color: #000;margin-top: 0px;margin-bottom: 0px;padding-left: 285px;">What is Lorem Ipsum?</h3>
                                <p style="margin-bottom: 0px;color: #000;padding-left: 285px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                    <!--/.Slides-->
                    <!--Controls-->
                    <a class="carousel-control-prev icon" href="#carousel-example-2" role="button" data-slide="prev">
                        <i class="fas fa-angle-left"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next icon" href="#carousel-example-2" role="button" data-slide="next">
                        <i class="fas fa-angle-right"></i>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--/.Controls-->
                </div>
            </div>
        </div>
        <!--/.Carousel Wrapper-->
    </section>
</main>